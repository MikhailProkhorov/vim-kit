# vim-kit
## Переключение конфигов vim
Изучение конфигурации vim, какой-то момент может начать занимать слишком много времени. При этом, хотелось бы
одновременно иметь доступ как к старой и хорошо знакомой конфигурации, так и попробовать, что-то новое.
Большенство разработчиков таких конфигураций предлагают сразу удалить существующую конфигурацию ./.config/nvim и заменить её на то,
что они предлагают. Но при этом происходит потеря готовой, проверенной и привычной конфигурации. ## Как использоватть разные конфиги vim.

Можно просто устанавливать конфиг в отдельную папку и при каждом вызове nvim, явно указывать, какую конфигурацию
использовать.

```NVIM_APPNAME=nvim-lazyvim nvim```
Neovim использует переменную NVIM_APPNAME для определения директории в которой находятся файлы конфигурации.
Если данная переменная не определена или имеет не валидное значение, будет использованао значение ~/.config/nvim.

---
Все ниже приведённые настройки приведены для shel окружения zsh. Наверняка могут быть применены и для других.
Welcome for maintaince.

---

### Способы переключения конфигурацй.

1. Назначение alias-ов
```~/.zshrc
alias v='nvim' # default Neovim config
alias vz='NVIM_APPNAME=nvim-lazyvim nvim' # LazyVim
alias vc='NVIM_APPNAME=nvim-nvchad nvim' # NvChad
alias vk='NVIM_APPNAME=nvim-kickstart nvim' # Kickstart
alias va='NVIM_APPNAME=nvim-astrovim nvim' # AstroVim
alias vl='NVIM_APPNAME=nvim-lunarvim nvim' # LunarVim
```
Из недостатков: придётся постоянно держать в голове конфиги и их alias-ы, а так же при необходимости обновлять их.
2. Можно не держать в голове alias-ы, а вместо этого ориентироваться на предустановленный списко.
Разместить скрипт ниже, в вашем .zshrc
```~/.zshrc
vv() {
  select config in lazyvim kickstart nvchad astrovim lunarvim
  do NVIM_APPNAME=nvim-$config nvim $@; break; done
}
```
После этого выполните ```source ~/.zshrc```. Теперь у вас доступна команда vv, которая покажет список предустановленных
конфигураций и после выбора запустит nvim с соответствующей конфигурацией
Из недостатков, нужно будет вручную поддерживать актуальность скрипта и следить за соответствием названий в скрипте и
названий директорий с конфигурациями.
3. Использовать дополнительные пакеты fzf и fd-find (в Ubuntu) / fd (в других дистрибутивах).
```$ sudo apt install fzf fd-find```
```~/.zshrc
# NeoVim Switch Config
vv() {
  # Assumes all configs exist in directories named ~/.config/nvim-*
  local config=$(fdfind --max-depth 1 --glob 'nvim*' ~/.config | fzf --prompt="Neovim Configs > " --height=50% --layout=reverse --border --exit-0)

  # If I exit fzf without selecting a config, don't open Neovim
  [[ -z $config ]] && echo "No config selected" && return

  # Open Neovim with the selected config
  NVIM_APPNAME=$(basename $config) nvim $@
}
```

Для установки склонируйте репозиторий в диркеторию ~/opt/ ```bash$ git clone <repo_path>```
 
и выполните:
 

```bash$ ln -s ~/opt/nvim-kit/.config/nvim ~/.config/nvim-kit ```

## Branches

- basic: установлен менеджер пакетов lazy.nvim. Если есть желание настроить nvim "с нуля" - это отличное место для старта

## Применённые настройки

## Переназначенные комбинации клавишь

## Пралагины

[Список плагинов, в основном lua, для дальнейшей настройки](https://github.com/rockerBOO/awesome-neovim#workflow)
