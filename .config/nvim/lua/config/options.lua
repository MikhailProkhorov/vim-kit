local options = {
    encoding = 'utf-8',
    fileencodings = { 'utf-8', 'cp1251' },
    number = true,
    clipboard = 'unnamedplus',

    syntax = 'enable',
    
    -- Searching
	ignorecase = true, -- case insensitive searching
	smartcase = true, -- case-sensitive if expresson contains a capital letter
	inccommand = 'nosplit', -- neovim feature: live preview
    hlsearch = true,
    gdefault = true, -- use 'g' flag by default with :s/foo/bar
    incsearch = true,

    mouse = 'a',
    autoindent = true,
    smartindent = true,
    splitbelow = true,
    splitright = true,

    -- For file watchers
    swapfile = false,
    backupdcopy = treu,
    backup = false,
    writebackup = false,
    exrc = true,
    secure = true,

    joinspaces = false, -- Prevents inserting two spaces after punctuation on a join (J)

    termguicolors = true,
    guifont = 'FiraCode Nerd Font Regular:h16',
    undofile = true,
    expandtab = true,
    tabstop = 4,
    shiftwidth = 4,
    softtabstop = 4,
    cursorline = true,
    scrolloff = 8,
    showmatch = true,
    compatible = false,
    wrap = false,
    -- Default nvim statusline (If statusline plugin disabled)
    statusline = '▌%{toupper(mode())}▐ %F%m%r%h%w │ %2p%% %l/%L %-2v │ ts:%{&ts} sw:%{&sw} ft:%Y ff:%{&ff} │ %{&encoding}',
    shortmess = 'ilmnrxsWF', -- helps to avoid all the hit-enter prompts caused by file messages
	showmode = true,

    formatoptions = 'mBo',

    fillchars = { -- :h 'fcs'
		vert = '│', -- vertical separator for window border
		diff = '╱', -- deleted lines for diff view
	},

    matchpairs = { -- Press % to jump from one to the other. :h 'matchpairs'
		-- LuaFormatter off
		'(:)', '{:}', '[:]', '「:」', '<:>', '“:”',
		-- LuaFormatter on
	},

    -- Folding
    foldmethod = 'manual',
    foldnestmax = 10,
    foldlevel = 2,
    colorcolumn = '120'
}

for k, v in pairs(options) do
    vim.opt[k] = v
end

